FROM java:8

ADD target/spring-boot-web-0.0.1-SNAPSHOT.jar spring-boot-web-0.0.1-SNAPSHOT.jar

EXPOSE 8080

CMD ["java","-jar","spring-boot-web-0.0.1-SNAPSHOT.jar"]